// functions --------------------------------------------------------------

// toggle active class
// evt: an event
const toggleActive = evt => {
  const activeOlds = document.querySelectorAll(".active");
  const activeNew = document.querySelector(evt.target.dataset.linked);
  activeOlds.forEach(old => old.classList.remove("active"));
  evt.target.classList.add("active");
  activeNew.classList.add("active");
};

// populate options of a select form
// select: a select form
// arr: an array of objects containing the option data (i.e. text and value)
const populateSelect = (select, arr) => {
  arr.forEach(obj => {
    const opt = document.createElement("option");
    opt.value = obj.value;
    opt.text = obj.text;
    select.append(opt);
  });
};

// toggle selected options of a select form
// select: a select form
// arr: an array of the value to select
const toggleSelected = (select, arr) => {
  const opts = Array.from(select.options);
  const idxs = arr.map(val => opts.findIndex(opt => opt.value === val));
  idxs.forEach(idx => select[idx].selected = !select[idx].selected);
};

// get selected options from a select form
// select: a select form
// val: a boolean indicating whether to return value or text
const getSelected = (select, val) => {
  const selOpts = Array.from(select.selectedOptions);
  if (val) {
    return selOpts.map(opt => opt.value);
  } else {
    return selOpts.map(opt => opt.text);
  }
};

// make legend items in alphabetical order
// (in alphabetical order rather than in the order of arr so that fgClass are set in a predictable order, e.g. to match those in highlightSeries())
// list: a list where to add the legend items
// arr: an array of the text for the legend items
// locales: see localeCompare()
// fgClass: an array of classes to apply to the legend items
const makeLegend = (list, arr, locales, fgClass) => {
  list.innerHTML = "";
  arr
    .sort((ref, comp) => ref.localeCompare(comp, locales))
    .forEach((text, idx) => {
      const li = document.createElement("li");
      li.classList.add(fgClass[idx]);
      li.textContent = text;
      list.append(li);
    });
};

// reorder .ct-series in the DOM and set classes in alphabetical order
// (in the DOM because no z-index for svg elements)
// (in alphabetical order rather than in the order of the DOM so that fgClass are set in a predictable order, e.g. to match those in makeLegend())
// elm: an element containing a Chartist chart
// arr: an array of classes indicating the foreground series
// locales: see localeCompare()
// bgClass: a class to apply to the background series
// fgClass: an array of classes to apply to the foreground series
const highlightSeries = (elm, arr, locales, fgClass, bgClass) => {
  const parent = elm.querySelector(".ct-chart-line > g:not(.ct-grids, .ct-labels)");
  const fgQuery = arr.map(clss => `.ct-series.${clss}`).join(", ");
  const fgSeries = elm.querySelectorAll(fgQuery);
  const arrStr = arr.join(", .");
  const bgQuery = `.ct-series:not(.${arrStr})`;
  const bgSeries = elm.querySelectorAll(bgQuery);
  const series = elm.querySelectorAll(".ct-series");
  parent.innerHTML = "";
  parent.append(...bgSeries);
  series.forEach(serie => serie.classList.remove(...fgClass, bgClass));
  Array.from(fgSeries)
    .sort((ref, comp) => ref.classList[1].localeCompare(comp.classList[1], locales))
    .forEach((serie, idx) => {
      serie.classList.add(fgClass[idx]);
      parent.append(serie);
    });
  bgSeries.forEach(serie => serie.classList.add(bgClass));
};

// add tooltips to .ct-series
// elm: an element containing a Chartist chart
const makeTooltip = elm => {
  const series = elm.querySelectorAll(".ct-series");
  series.forEach(serie => {
    const title = document.createElementNS("http://www.w3.org/2000/svg", "title");
    title.textContent = serie.getAttribute("ct:series-name");
    serie.append(title);
  });
};

// populate table rows
// table: a table
// arr: an array of objects containing the row data
const populateTable = (table, arr) => {
  arr.forEach(obj => {
    const row = table.insertRow();
    Object.values(obj).forEach(val => {
      const cell = row.insertCell();
      cell.textContent = val;
    });
    table.append(row);
  });
};

// format table columns using Intl.NumberFormat()
// table: a table
// idx: the index of the column to format
// locales: see Intl.NumberFormat()
// options: see Intl.NumberFormat()
const formatCol = (table, idx, locales, options) => {
  const rows = table.querySelectorAll("tr");
  rows.forEach(row => {
    const cell = row.children[idx];
    cell.textContent = new Intl.NumberFormat(locales, options).format(cell.textContent);
  });
};

// filter table based on the first column
// table: a table
// arr: an array of the values to filter
const filterTable = (table, arr) => {
  const rows = table.querySelectorAll("tr");
  const selectedOlds = document.querySelectorAll(".selected");
  selectedOlds.forEach(old => old.classList.remove("selected"));
  rows.forEach(row => {
    const firstCell = row.firstChild;
    if (arr.includes(firstCell.textContent)) {
      row.classList.add("selected");
    }
  });
};

// variables --------------------------------------------------------------

const loc = "en-US";

const tabs = document.querySelectorAll("nav li");

const drugs = document.querySelector("#drugs");
const drugsTextInit = ["Clonazepam", "Fentanyl", "Oxycodone", "Pregabalin", "Tramadol"];
const drugsValInit = drugsTextInit.map(text => text.toLowerCase());

const legend = document.querySelector(".legend");
const chartDivs = document.querySelectorAll(".ct-perfect-fourth");
const suffixes = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"];
const fgCols = suffixes.map(suffix => `foreground-${suffix}`);
const bgCol = "background";

const tbody = document.querySelector("tbody");
const numCols = [2, 3, 4];
const fmt = { "maximumFractionDigits": 2, "minimumFractionDigits": 2 };

// init -------------------------------------------------------------------

// wait for both fetch to avoid getting selected values before the select form is populated
// do not wait between chart building and the event handler to avoid missing the first "created" event
// use getSelected() rather than drugsValInit in the event handler because "created" is emitted each time the chart is (re)drawn (e.g. chart creation, window resize, etc.)
Promise.all([
  fetch("data/select.json").then(response => response.json()),
  fetch("data/chartist.json").then(response => response.json())
])
  .then(data => {
    populateSelect(drugs, data[0]);
    toggleSelected(drugs, drugsValInit);
    makeLegend(legend, drugsTextInit, loc, fgCols);
    
    Object.values(data[1]).forEach((val, idx) => {
      const chart = new Chartist.LineChart(
        chartDivs[idx],
        { "series": val },
        {
          "axisX": {
            "onlyInteger": true,
            "type": Chartist.AutoScaleAxis
          },
          "lineSmooth": false
        }
      );
      chart.on(
        "created",
        () => {
          const drugsVal = getSelected(drugs, true);
          const series = chartDivs[idx].querySelectorAll(".ct-series");
          makeTooltip(chartDivs[idx]);
          highlightSeries(chartDivs[idx], drugsVal, loc, fgCols, bgCol);
          series.forEach(serie =>
            serie.addEventListener(
              "click",
              clickEvt => {
                const clickedClass = clickEvt.target.parentNode.classList[1];
                const changeEvt = new Event("change");
                toggleSelected(drugs, [clickedClass]);
                drugs.dispatchEvent(changeEvt);
              },
              false
            )
          );
        }
      );
    });
  });

fetch("data/table.json")
  .then(response => response.json())
  .then(data => {
    populateTable(tbody, data);
    numCols.forEach(col => formatCol(tbody, col, loc, fmt));
    filterTable(tbody, drugsTextInit);
  });

// events -----------------------------------------------------------------

tabs.forEach(tab => tab.addEventListener("click", toggleActive, false));

drugs.addEventListener(
  "change",
  () => {
    // this warns but does not prevent to (un)select options
    if (drugs.selectedOptions.length === 0) {
      alert("Select at least one drug.");
    } else if (drugs.selectedOptions.length > 10) {
      alert("Select no more than 10 drugs.");
    } else {
      const drugsText = getSelected(drugs, false);
      const drugsVal = getSelected(drugs, true);
      makeLegend(legend, drugsText, loc, fgCols);
      chartDivs.forEach(elm => highlightSeries(elm, drugsVal, loc, fgCols, bgCol));
      filterTable(tbody, drugsText);
    }
  },
  false
);
